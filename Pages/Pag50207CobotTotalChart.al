page 50207 CobotTotalChart
{
    PageType = CardPart;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = "Business Chart Buffer";

    layout
    {
        area(Content)
        {
            group(GroupName)
            {
                CaptionML = ENU = 'Total Production Status';
                usercontrol(Chart; "Microsoft.Dynamics.Nav.Client.BusinessChart")

                {
                    ApplicationArea = All;

                    trigger AddInReady()
                    begin
                        UpdateChart;
                    end;

                    trigger Refresh()
                    begin
                        UpdateChart;
                    end;
                }
            }
            usercontrol(Timer; TimerControl)
            {
                //Use generic control to add timer too.
                ApplicationArea = All;
                trigger ControlAddInReady()
                begin

                    CurrPage.Timer.SetTimerInterval(1000);
                    CurrPage.timer.StartTimer();
                end;

                trigger OnTimerElapsed()
                var

                begin
                    UpdateChart;

                end;
            }
        }
    }


    local procedure UpdateChart();
    var
        BusinessChartBuffer: Record "Business Chart Buffer";
    begin

        CLEAR(BusinessChartBuffer);
        WITH BusinessChartBuffer DO BEGIN
            Initialize;

            CalcValues;

            //Set Y-Axis
            AddMeasure('Count', 1, "Data Type"::Integer, "Chart Type"::Doughnut);

            //Set X-Axis
            SetXAxis('Quantity', "Data Type"::String);

            //1.
            BusinessChartBuffer.AddColumn('Scrap');
            BusinessChartBuffer.SetValue('Count', 0, TotalScrapQty);

            //2.
            BusinessChartBuffer.AddColumn('Succes');
            BusinessChartBuffer.SetValue('Count', 1, TotalOutputQty);

            //3.
            BusinessChartBuffer.AddColumn('Total');
            BusinessChartBuffer.SetValue('Count', 2, Total);
        END;
        if DataChanged then
            BusinessChartBuffer.Update(CurrPage.Chart);


    end;

    local procedure CalcValues();
    var
        CobotCue: Record CobotCue;
    begin
        CobotCue.Get();
        OldTotalOutputQty := TotalOutputQty;
        OldTotalScrapQty := TotalScrapQty;
        CobotCue.CalcFields("Total Scrap Quantity", "Total Output Quantity");
        TotalScrapQty := CobotCue."Total Scrap Quantity";
        TotalOutputQty := CobotCue."Total Output Quantity";
        Total := TotalScrapQty + TotalOutputQty;
        if (OldTotalOutputQty <> TotalOutputQty) OR (OldTotalScrapQty <> TotalScrapQty) then
            DataChanged := true
        else
            DataChanged := false;
    end;

    var
        TotalScrapQty: Decimal;
        TotalOutputQty: Decimal;
        Total: Decimal;
        OldTotalScrapQty: Decimal;
        OldTotalOutputQty: Decimal;
        DataChanged: Boolean;
}