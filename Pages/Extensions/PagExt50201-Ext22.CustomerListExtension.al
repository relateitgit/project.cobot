pageextension 50201 CustomerListExtension extends "Customer List"
{
    actions
    {
        AddLast("&Customer")
        {
            action("Start Json Receiver")
            {
                ApplicationArea = all;
                trigger OnAction();
                var
                    BilaWebservice: Codeunit "BilaWebservice";
                begin
                    BilaWebservice.RobotJsonReceiver('{"deviceid":"RobotArm","bottle_diff":-1,"type":"delivered","eventtoken":null,"time":"2018-10-03T06:45:05.9240000Z"}');
                end;
            }
        }
    }
}