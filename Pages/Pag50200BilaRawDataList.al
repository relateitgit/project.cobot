page 50200 "Bila Raw Data List"
{
    PageType = List;
    UsageCategory = Lists;
    ApplicationArea = All;
    SourceTable = "Bila Raw Data";
    SourceTableView = SORTING ("Entry No.") ORDER(descending);

    layout
    {
        area(Content)
        {
            repeater(Group)
            {
                field("Entry No."; "Entry No.")
                {
                    ApplicationArea = All;
                }
                field("Execution Time"; "Execution Time")
                {
                    ApplicationArea = All;

                }

                field(DeviceId; DeviceId)
                {
                    ApplicationArea = All;

                }

                field("Bottle Diff"; "Bottle Diff")
                {
                    ApplicationArea = All;

                }

                field("Type"; "Type")
                {
                    ApplicationArea = All;

                }

                field("Event Token"; "Event Token")
                {
                    ApplicationArea = All;

                }
                field(Processed; Processed)
                {
                    ApplicationArea = All;
                }
            }
        }

    }

    actions
    {
        area(Processing)
        {
            action(ActionName)
            {
                ApplicationArea = All;
                CaptionML = ENU = 'Process Record';


                trigger OnAction();
                begin
                    BilaEventPublishers.OnAfterInsertBilaRawDataRecord(Rec);
                    Message(text001);
                end;

            }

            action(ResetRec)
            {


                ApplicationArea = All;
                CaptionML = ENU = 'Reset Record';
                trigger OnAction();
                begin
                    ProdOrd.SetRange(Status, ProdOrd.Status::Released);
                    ProdOrd.SetRange(Blocked, false);
                    ProdOrd.FindFirst;
                    ProdLine.SetRange("Prod. Order No.", ProdOrd."No.");
                    ProdOrd.Blocked := true;
                    /**
                    if ProdLine.FindFirst() then
                        repeat
                            ProdLine2 := ProdLine;
                            ProdLine2.Delete(false);
                        until ProdLine.next = 0;
                    **/
                    ProdOrd.Modify(false);
                    Message(text001);
                end;

            }
        }
    }


    var
        BilaEventPublishers: Codeunit "Bila Event Publishers";
        ProdOrd: Record "Production Order";
        ProdLine: Record "Prod. Order Line";
        ProdLine2: Record "Prod. Order Line";
        text001: TextConst ENU = 'Process Done';


}