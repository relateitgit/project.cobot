page 50204 CobotActivities
{
    PageType = CardPart;
    ApplicationArea = All;
    UsageCategory = Lists;
    SourceTable = CobotCue;
    Caption = 'Flow Video';

    layout
    {
        area(Content)
        {
            cuegroup(Cues)
            {
                CaptionML = ENU = '';

                actions

                {
                    action(FlowWalkthrough)
                    {
                        ApplicationArea = all;
                        Image = TileVideo;
                        CaptionML = ENU = 'Flow Walk-through';
                        trigger OnAction()
                        var
                            VideoLink: Page VideoPlayer;
                        begin
                            //VideoLink.SetURL('https://youtu.be/dbJAq4_fp7g');
                            VideoLink.RUNMODAL;
                        end;
                    }
                }
            }
            cuegroup(TotalProductionOrderStatus)
            {

                CaptionML = ENU = 'Total Production Orders';
                field("Released Prod. Orders - All"; "Released Prod. Orders - All")
                {
                    CaptionML = ENU = 'Released';
                    ApplicationArea = All;


                }

                field("Posted Prod. Orders - All"; "Posted Prod. Orders - All")
                {
                    CaptionML = ENU = 'Posted';
                    ApplicationArea = All;
                    Visible = false;
                }



            }

            cuegroup(ProductionOrderStatus2)
            {
                CaptionML = ENU = 'Current Production Status';
                field("Current Capacity Entries"; Quantity)
                {
                    CaptionML = ENU = 'Quantity';
                    ApplicationArea = All;
                    DecimalPlaces = 0 : 0;
                    Visible = false;
                }

                field("Output Quantity"; OutputQuantity)
                {
                    CaptionML = ENU = 'Succes';
                    ApplicationArea = All;
                    DecimalPlaces = 0 : 0;
                }
                field("Scrap Quantity"; ScrapQuantity)
                {
                    ApplicationArea = All;
                    CaptionML = ENU = 'Scrap';
                    DecimalPlaces = 0 : 0;
                }
                field(Expected; ExpectedFinished)
                {
                    ApplicationArea = All;
                    CaptionML = ENU = 'Expected Output';
                }
            }

            usercontrol(Timer; TimerControl)
            {
                //Use generic control to add timer too.
                ApplicationArea = All;
                trigger ControlAddInReady()
                begin
                    ControlIsReady := true;
                    CurrPage.Timer.SetTimerInterval(1000);
                    CurrPage.timer.StartTimer();
                end;

                trigger OnTimerElapsed()
                var

                begin
                    /**
                    OldReleasedProdOrders := "Released Prod. Orders - All";
                    OldPostedProdOrders := "Posted Prod. Orders - All";
                    OldQuantity := Quantity;
                    OldScrapQuantity := "Scrap Quantity";
                    OldOutputQuantity := "Output Quantity";
                    
                    FindFirst;
                    CalcFields("Released Prod. Orders - All");
                    CalcFields("Posted Prod. Orders - All");
                    CalcFields("quantity");
                    CalcFields("Scrap Quantity");
                    CalcFields("Output Quantity");
                    **/
                    OldReleasedProdOrders := "Released Prod. Orders - All";
                    OldPostedProdOrders := "Posted Prod. Orders - All";
                    OldQuantity := Quantity;
                    OldScrapQuantity := ScrapQuantity;
                    OldOutputQuantity := OutputQuantity;

                    FindFirst;
                    CalcFields("Released Prod. Orders - All");
                    CalcFields("Posted Prod. Orders - All");
                    Quantity := CoBotCue.GetQuantityUsed();
                    ScrapQuantity := CoBotCue.GetScrapQuantity();
                    OutputQuantity := CoBotCue.GetOutputQuantity();

                    if (OldReleasedProdOrders <> "Released Prod. Orders - All") Or
                       (OldPostedProdOrders <> "Posted Prod. Orders - All") or
                       (OldQuantity <> Quantity) or
                       (OldScrapQuantity <> "Scrap Quantity") or
                       (OldOutputQuantity <> "Output Quantity") then
                        CurrPage.update(false);
                end;
            }
        }
    }

    trigger OnOpenPage();
    begin
        CRLF[1] := 13;
        CRLF[2] := 10;
        Reset();
        if not get then begin
            init;
            Insert();
        end;
    end;

    local procedure ExpectedFinished(): Integer
    begin
        exit(12 - ScrapQuantity);
    end;


    local procedure MyProcedure()
    begin

    end;

    local procedure Test()
    begin
        ;
    end;

    var
        CoBotCue: Record CobotCue;
        ControlIsReady: Boolean;
        CRLF: text[2];
        BilaEvent: codeunit "Bila Event Publishers";
        OldReleasedProdOrders: Decimal;
        OldPostedProdOrders: Decimal;
        OldQuantity: Decimal;
        OldScrapQuantity: Decimal;
        OldOutputQuantity: Decimal;
        OutputQuantity: Decimal;
        ScrapQuantity: Decimal;
        Quantity: Decimal;

}