page 50206 CobotChart
{
    PageType = CardPart;
    ApplicationArea = All;
    UsageCategory = Administration;
    SourceTable = "Business Chart Buffer";

    layout
    {
        area(Content)
        {
            group(GroupName)
            {
                CaptionML = ENU = 'Current Production Status';
                usercontrol(Chart; "Microsoft.Dynamics.Nav.Client.BusinessChart")

                {
                    ApplicationArea = All;

                    trigger AddInReady()
                    begin
                        isPageLoad := true;
                        UpdateChart;
                    end;

                    trigger Refresh()
                    begin
                        UpdateChart;
                    end;
                }
            }
            usercontrol(Timer; TimerControl)
            {
                //Use generic control to add timer too.
                ApplicationArea = All;
                trigger ControlAddInReady()
                begin

                    CurrPage.Timer.SetTimerInterval(1000);
                    CurrPage.timer.StartTimer();
                end;

                trigger OnTimerElapsed()
                var

                begin
                    UpdateChart;

                end;
            }
        }
    }


    local procedure UpdateChart();
    var
        BusinessChartBuffer: Record "Business Chart Buffer";
    begin

        CLEAR(BusinessChartBuffer);
        WITH BusinessChartBuffer DO BEGIN
            Initialize;

            CalcValues;

            //Set Y-Axis
            AddMeasure('Count', 1, "Data Type"::Integer, "Chart Type"::Column);

            //Set X-Axis
            SetXAxis('Quantity', "Data Type"::String);

            //1.
            BusinessChartBuffer.AddColumn('Scrap');
            BusinessChartBuffer.SetValue('Count', 0, ScrapQty);


            //2.
            BusinessChartBuffer.AddColumn('Succes');
            BusinessChartBuffer.SetValue('Count', 1, OutputQty);

            //3.
            BusinessChartBuffer.AddColumn('Expected');
            BusinessChartBuffer.SetValue('Count', 2, ExpectedQty);
        END;

        if DataChanged or isPageLoad then begin
            BusinessChartBuffer.Update(CurrPage.Chart);
            isPageLoad := false;
        end;

    end;

    local procedure CalcValues();
    var
        CobotCue: Record CobotCue;
    begin
        CobotCue.Get();
        OldScrapQty := ScrapQty;
        OldOutputQty := OutputQty;
        //CobotCue.CalcFields("Scrap Quantity", "Output Quantity");
        //ScrapQty := CobotCue."Scrap Quantity";
        //OutputQty := CobotCue."Output Quantity";
        //ExpectedQty := 12 - ScrapQty;
        ScrapQty := CobotCue.GetScrapQuantity();
        OutputQty := CobotCue.GetOutputQuantity();
        ExpectedQty := 12 - ScrapQty;
        if (OldScrapQty <> ScrapQty) OR (OldOutputQty <> OutputQty) then
            DataChanged := true
        else
            DataChanged := false;

    end;


    var
        ScrapQty: Decimal;
        OutputQty: Decimal;
        ExpectedQty: Decimal;
        OldScrapQty: Decimal;
        OldOutputQty: Decimal;
        DataChanged: Boolean;
        isPageLoad: Boolean;
}