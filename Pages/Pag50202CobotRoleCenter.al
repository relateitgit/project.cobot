page 50202 CobotRoleCenter


{

    PageType = RoleCenter;
    ApplicationArea = All;
    UsageCategory = Administration;


    layout
    {


        area(RoleCenter)
        {


            group(MyGroup)
            {
                part(HeadlinePart; RobotCenterHeadlinePart)
                {
                    ApplicationArea = All;

                }
                part(ActivityPart; CobotActivities)
                {
                    ApplicationArea = All;

                }


            }
            group(Charts)
            {
                part(CobotChart; CobotChart)
                {
                    ApplicationArea = All;
                    CaptionML = ENU = 'Current Cobot Order';

                }
                part(CobotTotalChart; CobotTotalChart)
                {
                    ApplicationArea = All;
                    CaptionML = ENU = 'Total Cobot Orders';

                }
            }
            group("Power BI")
            {
                part(PowerBI; "Power BI Report Spinner Part")
                {
                    ApplicationArea = All;

                }
                part(PowerBI2; "Power BI Report Spinner Part")
                {
                    ApplicationArea = All;

                }
            }
        }


    }

    actions
    {
        area(Processing)
        {
            action(Reset)
            {
                ApplicationArea = All;
                RunObject = codeunit 50205;

            }
        }
    }

}

