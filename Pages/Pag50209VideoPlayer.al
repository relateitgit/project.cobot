Page 50209 VideoPlayer
{

    PageType = Card;
    CaptionML = ENU = 'Video Player';
    layout
    {
        area(Content)
        {
            usercontrol(GenericControlAddIn; GenericControlAddIn)
            {
                ApplicationArea = All;
                trigger ControlAddInReady()
                var
                    html: text;
                begin
                    ControlIsReady := true;

                    html := '<script>';
                    html += 'function setImage(data) {document.getElementById("gifanim").src = data.src;}';
                    html += '</script>';
                    html += '<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>';
                    html += '<iframe id="video" width="100%" height="100%" src="//www.youtube.com/embed/sYTIhCmPakI?rel=0" frameborder="0" allowfullscreen></iframe>';
                    CurrPage.GenericControlAddIn.LoadBody(html);
                    SetImage('https://media.giphy.com/media/11MObX3yzDkJbO/giphy.gif');
                end;
            }
        }
    }


    procedure SetImage(URL: Text)
    begin
        //CurrPage.GenericControlAddIn.InvokeMethod('setImage', '{"src":"' + URL + '"}');
        CurrPage.update(false);
    end;

    var
        ControlIsReady: Boolean;
}