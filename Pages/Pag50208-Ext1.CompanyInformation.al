pageextension 50208 CompanyInformation extends "Company Information"
{
    layout
    {
        // Add changes to page layout here
    }

    actions
    {
        // Add changes to page actions here
        addlast(Processing)
        {
            group(Robotics)
            {
                action(TEST)
                {
                    Promoted = true;
                    PromotedIsBig = true;
                    PromotedCategory = Process;
                    image = CustomerRating;
                    ApplicationArea = All;
                    Caption = 'TEST';
                    trigger OnAction();
                    var
                        CapacityLedgerEntry: Record "Capacity Ledger Entry";
                    begin
                        //CapacityLedgerEntry.SetFilter("Completely Invoiced" Quantity", '<>%1', 0);
                        CapacityLedgerEntry.ModifyAll("Completely Invoiced", true, false);
                        /*
                        CapacityLedgerEntry.SetRange("Item No.", '2002');
                        CapacityLedgerEntry.SetFilter("Output Quantity", '<>%1', 0);
                        if CapacityLedgerEntry.FindSet then begin
                            CapacityLedgerEntry.SetRange("Item No.");
                            CapacityLedgerEntry.ModifyAll("Output Quantity", 0);
                            message('Output Qty. set to 0')
                        end else begin
                            CapacityLedgerEntry.SetRange("Item No.");
                            CapacityLedgerEntry.ModifyAll("Output Quantity", 1);
                            message('Output Qty. set to 1')
                        end;
                        */
                    end;
                }
            }
        }
    }

    var
        myInt: Integer;
}
