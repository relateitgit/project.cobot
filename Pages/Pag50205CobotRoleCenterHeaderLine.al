page 50205 RobotCenterHeadlinePart
{
    PageType = HeadLinePart;
    ApplicationArea = All;
    UsageCategory = Administration;

    layout
    {
        area(content)
        {
            field(Headline1; text001)
            {
                ApplicationArea = All;
            }
            /*
            field(Headline2; text002)
            {
                ApplicationArea = All;
            }
            field(Headline3; text003)
            {
                ApplicationArea = All;
            
            }
            */
            field(Headline4; text004)
            {
                ApplicationArea = All;
            }
        }
    }

    var
        text001: TextConst ENU = 'Business Central Cobots';
        text002: TextConst ENU = 'This is headline 2';
        text003: TextConst ENU = 'This is headline 3';
        text004: TextConst ENU = 'Contact RelateIT for BC Robotics ';
}