codeunit 50205 Reset
{
    trigger OnRun()
    begin
        ProdOrd.SetRange(Status, ProdOrd.Status::Released);
        ProdOrd.SetRange(Blocked, false);
        ProdOrd.FindFirst;
        ProdLine.SetRange("Prod. Order No.", ProdOrd."No.");
        ProdOrd.Blocked := true;
        ProdOrd.Modify(false);
        Message('Done.');
    end;

    var

        ProdOrd: Record "Production Order";
        ProdLine: Record "Prod. Order Line";
}