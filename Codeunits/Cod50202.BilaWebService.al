codeunit 50202 BilaWebService
{
    trigger OnRun()
    begin

    end;

    procedure RobotJsonReceiver(jsonString: Text): Text
    begin
        OnAfterJsonReceived(jsonString);
    end;

    [IntegrationEvent(false, false)]
    local procedure OnAfterJsonReceived(jsonString: Text)
    begin

    end;

}