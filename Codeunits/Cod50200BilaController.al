codeunit 50200 "Bila Controller"
{
    trigger OnRun()
    begin

    end;

    procedure CreateRecordsFromJson(JsonResult: Text)
    var
        BilaController: Codeunit "Bila Controller";
        JsonTok: JsonToken;
        JsonValTok: JsonToken;
        BilaRawData: Record "Bila Raw Data";
        TextToCode: Text;
        BilaEventPublishers: Codeunit "Bila Event Publishers";

    begin
        JsonTok.ReadFrom(JsonResult);
        BilaRawData.init;
        JsonTok.SelectToken('time', JsonValTok);
        BilaRawData."Execution Time" := JsonValTok.AsValue().AsDateTime();
        JsonTok.SelectToken('deviceid', JsonValTok);
        BilaRawData.DeviceId := JsonValTok.AsValue().AsText();
        JsonTok.SelectToken('bottle_diff', JsonValTok);
        BilaRawData."Bottle Diff" := JsonValTok.AsValue().AsDecimal();
        JsonTok.SelectToken('type', JsonValTok);
        case JsonValTok.AsValue().AsText() of
            'delivered':
                begin
                    BilaRawData.Type := BilaRawData.Type::Delivered;
                    if BilaRawData."Bottle Diff" = 0 then
                        BilaRawData.Type := BilaRawData.Type::Handout;
                end;
            'empty':
                BilaRawData.Type := BilaRawData.Type::Empty;
            'handout':
                BilaRawData.Type := BilaRawData.Type::Handout;
        end;
        JsonTok.SelectToken('eventtoken', JsonValTok);
        JsonValTok.WriteTo(TextToCode);
        BilaRawData."Event Token" := TextToCode;
        BilaRawData.Insert(true);
        Commit();
        BilaEventPublishers.OnAfterInsertBilaRawDataRecord(BilaRawData);
    end;

    procedure HandleDeliveredBilaRawDataRecord(Rec: record "Bila Raw Data")
    var
        ItemJournalLine: Record "Item Journal Line";
        ProdOrd: Record "Production Order";
        "ItemJnlPostBatch": Codeunit "Item Jnl.-Post Batch";
        lineNo: Integer;
    begin
        ProdOrd.SetRange(Status, ProdOrd.Status::Released);
        ProdOrd.SetRange(Blocked, false);
        ProdOrd.FindFirst;
        /**
        clear(ItemJournalLine);
        ItemJournalLine."Journal Template Name" := 'AFGANG';
        ItemJournalLine."Journal Batch Name" := 'STANDARD';

        ItemJournalLine.validate("Item No.", '2002');
        ItemJournalLine."Order Type" := ItemJournalLine."Order Type"::Production;
        ItemJournalLine.validate("Order No.", ProdOrd."No.");
        ItemJournalLine.validate("Posting Date", WorkDate());
        ItemJournalLine.validate("Entry Type", ItemJournalLine."Entry Type"::Output);
        ItemJournalLine."Document No." := 'UP2001';
        ItemJournalLine."Source Type" := ItemJournalLine."Source Type"::Item;
        ItemJournalLine.VALIDATE("Source No.", '2002');
        ItemJournalLine.Validate("Gen. Bus. Posting Group", 'DANMARK');
        ItemJournalLine.Validate("Gen. Prod. Posting Group", 'DETAIL');
        ItemJournalLine.validate("Inventory Posting Group", 'HANDEL');
        ItemJournalLine.validate("Output Quantity", 1);
        ItemJournalLine.Insert(true);
        ItemJnlPostBatch.Run(ItemJournalLine);

        
**/
        clear(ItemJournalLine);
        ItemJournalLine."Journal Template Name" := 'AFGANG';
        ItemJournalLine."Journal Batch Name" := 'STANDARD';
        ItemJournalLine."Line No." := 10000;

        ItemJournalLine.validate("Item No.", '2002');
        ItemJournalLine."Order Type" := ItemJournalLine."Order Type"::Production;
        ItemJournalLine.validate("Order No.", ProdOrd."No.");
        ItemJournalLine.validate("Order Line No.", 10000);
        ItemJournalLine.validate("Posting Date", WorkDate());
        ItemJournalLine.validate("Entry Type", ItemJournalLine."Entry Type"::Output);
        ItemJournalLine."Document No." := 'UP2001';
        ItemJournalLine."Source Type" := ItemJournalLine."Source Type"::Item;
        ItemJournalLine.VALIDATE("Source No.", '2002');
        ItemJournalLine.Validate("Gen. Bus. Posting Group", 'DANMARK');
        ItemJournalLine.Validate("Gen. Prod. Posting Group", 'DETAIL');
        ItemJournalLine.validate("Inventory Posting Group", 'HANDEL');
        ItemJournalLine.validate("Scrap Quantity", 1);
        ItemJournalLine.Insert(true);
        ItemJnlPostBatch.Run(ItemJournalLine);

        TjeckFinishProdOrder(ProdOrd);

        Rec.validate(Processed, true);
        Rec.Modify(true);
    end;

    procedure HandleEmptyBilaRawDataRecord(Rec: record "Bila Raw Data")
    var
        myInt: Integer;
        ItemJournalLine: Record "Item Journal Line";
        ProdOrd: Record "Production Order";
        "ItemJnlPostBatch": Codeunit "Item Jnl.-Post Batch";
    begin
        ProdOrd.SetRange(Status, ProdOrd.Status::Released);
        ProdOrd.SetRange(Blocked, false);
        ProdOrd.FindFirst;

        clear(ItemJournalLine);
        ItemJournalLine."Journal Template Name" := 'AFGANG';
        ItemJournalLine."Journal Batch Name" := 'STANDARD';
        ItemJournalLine."Line No." := 10000;

        ItemJournalLine.validate("Item No.", '2002');
        ItemJournalLine."Order Type" := ItemJournalLine."Order Type"::Production;
        ItemJournalLine.validate("Order No.", ProdOrd."No.");
        ItemJournalLine.validate("Order Line No.", 10000);
        ItemJournalLine.validate("Posting Date", WorkDate());
        ItemJournalLine.validate("Entry Type", ItemJournalLine."Entry Type"::Output);
        ItemJournalLine."Document No." := 'UP2001';
        ItemJournalLine."Source Type" := ItemJournalLine."Source Type"::Item;
        ItemJournalLine.VALIDATE("Source No.", '2002');
        ItemJournalLine.Validate("Gen. Bus. Posting Group", 'DANMARK');
        ItemJournalLine.Validate("Gen. Prod. Posting Group", 'DETAIL');
        ItemJournalLine.validate("Inventory Posting Group", 'HANDEL');
        ItemJournalLine.validate("Scrap Quantity", 1);
        ItemJournalLine.Insert(true);
        ItemJnlPostBatch.Run(ItemJournalLine);

        TjeckFinishProdOrder(ProdOrd);

        Rec.validate(Processed, true);
        Rec.Modify(true);

    end;

    procedure HandleHandoutBilaRawDataRecord(Rec: record "Bila Raw Data")
    var

        ItemJournalLine: Record "Item Journal Line";
        ProdOrd: Record "Production Order";
        "ItemJnlPostBatch": Codeunit "Item Jnl.-Post Batch";
    begin
        ProdOrd.SetRange(Status, ProdOrd.Status::Released);
        ProdOrd.SetRange(Blocked, false);
        ProdOrd.FindFirst;
        /**
                clear(ItemJournalLine);

                ItemJournalLine."Journal Template Name" := 'CONSUMPTIO';
                ItemJournalLine."Journal Batch Name" := 'DEFAULT';

                ItemJournalLine.validate("Item No.", '2000');
                ItemJournalLine."Order Type" := ItemJournalLine."Order Type"::Production;
                ItemJournalLine.validate("Order No.", ProdOrd."No.");
                ItemJournalLine.validate("Order Line No.", 10000);
                ItemJournalLine.validate("Posting Date", WorkDate());
                ItemJournalLine.validate("Entry Type", ItemJournalLine."Entry Type"::Consumption);
                ItemJournalLine."Source Type" := ItemJournalLine."Source Type"::Item;
                ItemJournalLine.VALIDATE("Source No.", '2002');
                ItemJournalLine."Document No." := 'UP2001';
                ItemJournalLine.Validate("Gen. Bus. Posting Group", 'DANMARK');
                ItemJournalLine.Validate("Gen. Prod. Posting Group", 'DETAIL');
                ItemJournalLine.validate("Inventory Posting Group", 'HANDEL');
                ItemJournalLine.validate("Quantity", 1);
                ItemJournalLine.Insert;
                //ItemJnlPostBatch.Run(ItemJournalLine);

                clear(ItemJournalLine);
                ItemJournalLine."Journal Template Name" := 'CONSUMPTIO';
                ItemJournalLine."Journal Batch Name" := 'DEFAULT';

                ItemJournalLine.validate("Item No.", '2001');
                ItemJournalLine."Order Type" := ItemJournalLine."Order Type"::Production;
                ItemJournalLine.validate("Order No.", ProdOrd."No.");
                ItemJournalLine.validate("Order Line No.", 10000);
                ItemJournalLine.validate("Posting Date", WorkDate());
                ItemJournalLine.validate("Entry Type", ItemJournalLine."Entry Type"::Consumption);
                ItemJournalLine."Document No." := 'UP2001';
                ItemJournalLine."Source Type" := ItemJournalLine."Source Type"::Item;
                ItemJournalLine.VALIDATE("Source No.", '2002');
                ItemJournalLine.Validate("Gen. Bus. Posting Group", 'DANMARK');
                ItemJournalLine.Validate("Gen. Prod. Posting Group", 'DETAIL');
                ItemJournalLine.validate("Inventory Posting Group", 'HANDEL');
                ItemJournalLine.validate("Quantity", 1);
                ItemJournalLine.Insert;
                //ItemJnlPostBatch.Run(ItemJournalLine);
        **/
        clear(ItemJournalLine);
        ItemJournalLine."Journal Template Name" := 'AFGANG';
        ItemJournalLine."Journal Batch Name" := 'STANDARD';
        ItemJournalLine."Line No." := 10000;

        ItemJournalLine.validate("Item No.", '2002');
        ItemJournalLine."Order Type" := ItemJournalLine."Order Type"::Production;
        ItemJournalLine.validate("Order No.", ProdOrd."No.");
        ItemJournalLine.validate("Order Line No.", 10000);
        ItemJournalLine.validate("Posting Date", WorkDate());
        ItemJournalLine.validate("Entry Type", ItemJournalLine."Entry Type"::Output);
        ItemJournalLine."Document No." := 'UP2001';
        ItemJournalLine."Source Type" := ItemJournalLine."Source Type"::Item;
        ItemJournalLine.VALIDATE("Source No.", '2002');
        ItemJournalLine.Validate("Gen. Bus. Posting Group", 'DANMARK');
        ItemJournalLine.Validate("Gen. Prod. Posting Group", 'DETAIL');
        ItemJournalLine.validate("Inventory Posting Group", 'HANDEL');
        ItemJournalLine.validate("Output Quantity", 1);
        ItemJournalLine.Insert(true);

        ItemJnlPostBatch.Run(ItemJournalLine);

        TjeckFinishProdOrder(ProdOrd);

        Rec.validate(Processed, true);
        Rec.Modify(true);
    end;

    local procedure TjeckFinishProdOrder(ProdOrder: Record "Production Order")
    var
        CapEntry: Record "Capacity Ledger Entry";
        Qty: Integer;
        ProdChangeStatus: Codeunit "Prod. Order Status Management";
    begin
        CapEntry.SetRange("Order No.", ProdOrder."No.");
        Qty := CapEntry.Count();

        if Qty >= 12 then begin
            ProdChangeStatus.ChangeStatusOnProdOrder(ProdOrder, 4, WorkDate(), false);
        end;

    end;

}