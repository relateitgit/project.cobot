codeunit 50203 "Bila Event Publishers"
{
    [IntegrationEvent(true, true)]
    procedure OnAfterInsertBilaRawDataRecord(Rec: record "Bila Raw Data")
    begin
    end;

    [IntegrationEvent(true, true)]
    procedure OnInsertDeliveredBilaRawData(Rec: Record "Bila Raw Data")
    begin
    end;

    [IntegrationEvent(true, true)]
    procedure OnInsertEmptyBilaRawData(Rec: Record "Bila Raw Data")
    begin
    end;

    [IntegrationEvent(true, true)]
    procedure OnInsertHandoutBilaRawData(Rec: Record "Bila Raw Data")
    begin
    end;
}