codeunit 50201 "Bila Event Subscribers"
{
    [EventSubscriber(ObjectType::Codeunit, Codeunit::BilaWebService, 'OnAfterJsonReceived', '', false, false)]
    local procedure BilaJSONRecieved(jsonString: Text)
    var
        BilaController: Codeunit "Bila Controller";
    begin
        BilaController.CreateRecordsFromJson(jsonString);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Bila Event Publishers", 'OnAfterInsertBilaRawDataRecord', '', false, false)]
    local procedure OnAfterInsertRecordInBilaRawDataTable(Rec: Record "Bila Raw Data")
    var
        BilaEventPublisher: Codeunit "Bila Event Publishers";
    begin
        case rec.Type of
            rec.Type::Delivered:
                begin
                    BilaEventPublisher.OnInsertDeliveredBilaRawData(Rec);
                end;
            rec.Type::Empty:
                begin
                    BilaEventPublisher.OnInsertEmptyBilaRawData(Rec);
                end;
            rec.Type::Handout:
                begin
                    BilaEventPublisher.OnInsertHandoutBilaRawData(Rec);
                end;
        end;
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Bila Event Publishers", 'OnInsertDeliveredBilaRawData', '', false, false)]
    local procedure OnDeliveredBilaRecordInserted(Rec: Record "Bila Raw Data")
    var
        BilaController: Codeunit "Bila Controller";
    begin
        BilaController.HandleDeliveredBilaRawDataRecord(Rec);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Bila Event Publishers", 'OnInsertEmptyBilaRawData', '', false, false)]
    local procedure OnEmptyBilaRecordInserted(Rec: Record "Bila Raw Data")
    var
        BilaController: Codeunit "Bila Controller";
    begin
        BilaController.HandleEmptyBilaRawDataRecord(Rec);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Bila Event Publishers", 'OnInsertHandoutBilaRawData', '', false, false)]
    local procedure OnHandoutBilaRecordInserted(Rec: Record "Bila Raw Data")
    var
        BilaController: Codeunit "Bila Controller";
    begin
        BilaController.HandleHandoutBilaRawDataRecord(Rec);
    end;
}