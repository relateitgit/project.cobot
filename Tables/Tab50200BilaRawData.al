table 50200 "Bila Raw Data"
{
    DataClassification = ToBeClassified;

    fields
    {
        field(10; "Entry No."; Integer)
        {
            AutoIncrement = true;
            DataClassification = ToBeClassified;
        }
        field(20; "Execution Time"; DateTime)
        {
            DataClassification = ToBeClassified;

        }

        field(30; DeviceId; Text[30])
        {

        }

        field(40; "Bottle Diff"; Decimal)
        {

        }

        field(50; "Type"; option)
        {
            OptionMembers = "Empty","Delivered","Handout";
        }

        field(60; "Processed"; Boolean)
        {


        }

        field(5; "Event Token"; Code[10])
        {

        }
    }

    keys
    {
        key(PK; "Entry No.")
        {
            Clustered = true;
        }
    }

    trigger OnInsert()
    begin

    end;

    trigger OnModify()
    begin

    end;

    trigger OnDelete()
    begin

    end;

    trigger OnRename()
    begin

    end;

}