table 50202 CobotCue
{
    DataClassification = ToBeClassified;

    fields
    {
        field(1; "Primary Key"; Code[10])
        {
            DataClassification = ToBeClassified;
        }
        //Antal frigivne produktionsordere
        field(2; "Released Prod. Orders - All"; integer)
        {
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = Count("Production Order" WHERE (Status = CONST(Released),Blocked=CONST(false)));
        }
        //Bogførte produktions ordre
        field(4; "Posted Prod. Orders - All"; integer)
        {
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = Count ("Production Order" WHERE (Status = CONST (Finished)));
        }

        //Antal
        field(3; "Quantity"; Decimal)
        {
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = sum ("Capacity Ledger Entry".Quantity WHERE ("Completely Invoiced" = const (false)));
        }

        //Antal forbrugte
        field(6; "Output Quantity"; Decimal)
        {
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = sum ("Capacity Ledger Entry"."Output Quantity" WHERE ("Completely Invoiced" = const (false)));
        }

        //Antal spildt
        field(5; "Scrap Quantity"; Decimal)
        {
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = sum ("Capacity Ledger Entry"."Scrap Quantity" WHERE ("Completely Invoiced" = const (false)));
        }

        //Spildt antal i alt
        field(7; "Total Scrap Quantity"; Decimal)
        {
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = sum ("Capacity Ledger Entry"."Scrap Quantity");
        }

        //Antal forbrugte i alt
        field(8; "Total Output Quantity"; Decimal)
        {
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = sum ("Capacity Ledger Entry"."Output Quantity");
        }

    }

    //Calculated Fields by functions



    keys
    {
        key(PK; "Primary Key")
        {
            Clustered = true;
        }
    }

    trigger OnInsert()
    begin

    end;

    trigger OnModify()
    begin

    end;

    trigger OnDelete()
    begin

    end;

    trigger OnRename()
    begin

    end;

    procedure GetOutputQuantity(): Integer
    var
        ProdOrder: Record "Production Order";
        CapacityLedgerEntry: Record "Capacity Ledger Entry";
    begin
        ProdOrder.SetRange(Status, ProdOrder.Status::Released);
        ProdOrder.SetRange(Blocked, false);
        if ProdOrder.FindFirst then begin
            CapacityLedgerEntry.setrange("Order No.", ProdOrder."No.");
            CapacityLedgerEntry.CalcSums("Output Quantity");
            exit(CapacityLedgerEntry."Output Quantity");
        end;
        exit(0);
    end;

    procedure GetScrapQuantity(): Integer
    var
        ProdOrder: Record "Production Order";
        CapacityLedgerEntry: Record "Capacity Ledger Entry";
    begin
        ProdOrder.SetRange(Status, ProdOrder.Status::Released);
        ProdOrder.SetRange(Blocked, false);
        if ProdOrder.FindFirst then begin
            CapacityLedgerEntry.setrange("Order No.", ProdOrder."No.");
            CapacityLedgerEntry.CalcSums("Scrap Quantity");
            exit(CapacityLedgerEntry."Scrap Quantity");
        end;
        exit(0);
    end;

    procedure GetQuantityUsed(): Integer
    var
        ProdOrder: Record "Production Order";
        CapacityLedgerEntry: Record "Capacity Ledger Entry";
    begin
        ProdOrder.SetRange(Status, ProdOrder.Status::Released);
        ProdOrder.SetRange(Blocked, false);
        if ProdOrder.FindFirst then begin
            CapacityLedgerEntry.setrange("Order No.", ProdOrder."No.");
            CapacityLedgerEntry.CalcSums("Quantity");
            exit(CapacityLedgerEntry."Quantity");
        end;
        exit(0);
    end;

    /*

        local procedure GetCurrentProdOrderLine(): code[20]
        var
            ProdOrderLine: record "Prod. Order Line";
        begin
            ProdOrderLine.SetRange(Status, ProdOrderLine.status::Released);
        end;
    */

}