function SetTimerInterval(milliSeconds) {
    timerInterval = milliSeconds;
}
 
function StartTimer() {
    if (timerInterval == 0 || timerInterval == null) {
        Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('OnTimerError', ['No timer interval set.']);
        return;
    }
 
    timerObject = window.setInterval(ExecuteTimer, timerInterval);
}
 
function StopTimer() {
    clearInterval(timerObject);
}
 
function ExecuteTimer() {
    Microsoft.Dynamics.NAV.InvokeExtensibilityMethod('OnTimerElapsed', null);
}