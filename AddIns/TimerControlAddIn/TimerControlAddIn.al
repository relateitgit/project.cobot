controladdin TimerControl
{
    Scripts = 'AddIns/TimerControlAddIn/Scripts/MainScript.js';
    StartupScript = 'AddIns/TimerControlAddIn/Scripts/StartupScript.js';


    RequestedWidth = 1;
    RequestedHeight = 1;
    MinimumWidth = 1;
    MinimumHeight = 1;
    MaximumWidth = 1;
    MaximumHeight = 1;
    HorizontalShrink = true;
    VerticalShrink = true;
    HorizontalStretch = true;
    VerticalStretch = true;

    event ControlAddInReady();
    event OnTimerElapsed();
    event OnTimerError();
    procedure SetTimerInterval(milliSeconds: Integer)
    procedure StartTimer();
    procedure StopTimer();



}