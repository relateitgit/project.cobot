controladdin GenericControlAddIn
{
    //https://docs.microsoft.com/en-us/dynamics365/business-central/dev-itpro/developer/devenv-control-addin-object
    Scripts = 'AddIns/GenericControlAddIn/Scripts/MainScript.js', 'https://code.jquery.com/jquery-3.3.1.min.js';
    StartupScript = 'AddIns/GenericControlAddIn/scripts/StartupScript.js';
    StyleSheets = 'AddIns/GenericControlAddIn/styles/style.css';

    RequestedWidth = 300;
    RequestedHeight = 300;
    MinimumWidth = 250;
    MinimumHeight = 250;
    MaximumWidth = 500;
    MaximumHeight = 500;
    HorizontalShrink = true;
    VerticalShrink = true;
    HorizontalStretch = true;
    VerticalStretch = true;

    event ControlAddInReady();
    event OnGenericEvent(EventName: Text; JSONData: Text);
    procedure LoadBody(body: text);
    procedure InvokeMethod(MethodName: Text; JSONData: text);


}