page 50210 "Create Cobot Demo Data"
{
    PageType = Card;
    UsageCategory = Tasks;
    ApplicationArea = all;
    Editable = false;
    InsertAllowed = false;
    DeleteAllowed = false;
    ModifyAllowed = false;

    layout
    {
        area(content)
        {
            group("General")
            {
                CaptionML = ENU = 'General';
                group("Info")
                {
                    CaptionML = ENU = 'General';
                    InstructionalTextML = ENU = 'Setup Demo data, for RelateIt Cobot Project. This will Create Items, Production orders and Post Inventory';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action("Create Demo Data")
            {
                Image = "Setup";
                ApplicationArea = all;
                trigger OnAction();
                begin
                    Initrobot.Run;
                    Message('Done');
                end;
            }

            action("Create Demo ProdOrder")
            {
                Image = "Setup";
                ApplicationArea = all;
                trigger OnAction();
                begin
                    Initrobot.CreateProdOrder();
                    Message('Done');
                end;
            }
        }
    }

    var
        Initrobot: Codeunit "InitRobotDemo";
}