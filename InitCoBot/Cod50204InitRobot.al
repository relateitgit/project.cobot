codeunit 50204 InitRobotDemo
{
    trigger OnRun()
    begin
        CreateData();
    end;

    var
        myInt: Integer;

    local procedure CreateData()
    var
        "ItemRec": Record Item;
        "ProdBomHeader": Record "Production BOM Header";
        "ProdBomLine": Record "Production BOM Line";
        ItemJournalLine: Record "Item Journal Line";
        "ItemJnlPostBatch": Codeunit "Item Jnl.-Post Batch";
        ProdOrd: Record "Production Order";
        ProdLine: Record "Prod. Order Line";
        i: Integer;

    begin
        if not ItemRec.get('2000') then begin
            Clear(ItemRec);
            ItemRec."No." := '2000';
            ItemRec.Description := 'Water No label';
            ItemRec.validate("Unit Cost", 25);
            ItemRec.validate("Unit Price", 100);
            if ItemRec.Insert(true) then begin
                ItemRec.validate("Base Unit of Measure", 'STK');
                ItemRec.Modify;
            end;
        end;


        Clear(ItemRec);
        if not ItemRec.get('2001') then begin
            ItemRec."No." := '2001';
            ItemRec.Description := 'RelateIT Label';
            ItemRec.validate("Unit Cost", 25);
            ItemRec.validate("Unit Price", 100);
            ItemRec.validate("Gen. Prod. Posting Group", 'DETAIL');
            ItemRec.validate("Inventory Posting Group", 'HANDEL');
            if ItemRec.Insert(true) then begin
                ItemRec.validate("Base Unit of Measure", 'STK');
                ItemRec.Modify;
            end;
        end;

        Clear(ItemRec);
        if not ItemRec.get('2002') then begin
            ItemRec."No." := '2002';
            ItemRec.Description := 'RelateIt Water';
            ItemRec.validate("Unit Cost", 25);
            ItemRec.validate("Unit Price", 100);
            ItemRec.validate("Gen. Prod. Posting Group", 'DETAIL');
            ItemRec.validate("Inventory Posting Group", 'HANDEL');
            ItemRec.Insert(true);
            ItemRec.validate("Base Unit of Measure", 'STK');
            ItemRec.Modify;
        end;

        Clear(ProdBomHeader);
        ProdBomHeader."No." := '2002';
        ProdBomHeader.Description := 'RelateIt Water';
        ProdBomHeader.validate("Unit of Measure Code", 'STK');
        if ProdBomHeader.Insert(true) then;


        ProdBomLine."Production BOM No." := ProdBomHeader."No.";
        ProdBomLine."Line No." := 10000;
        ProdBomLine.validate(Type, ProdBomLine.Type::Item);
        ProdBomLine.validate("No.", '2000');
        ProdBomLine.Validate(Quantity, 1);
        ProdBomLine.validate("Quantity per", 1);
        if ProdBomLine.Insert(true) then;


        clear(ProdBomLine);
        ProdBomLine."Production BOM No." := ProdBomHeader."No.";
        ProdBomLine."Line No." := 20000;
        ProdBomLine.validate(Type, ProdBomLine.Type::Item);
        ProdBomLine.validate("No.", '2001');
        ProdBomLine.Validate(Quantity, 1);
        ProdBomLine.validate("Quantity per", 1);
        if ProdBomLine.Insert(true) then;


        ProdBomHeader.validate(Status, ProdBomHeader.Status::Certified);
        ProdBomHeader.Modify(true);

        Clear(ItemJournalLine);
        ItemJournalLine."Journal Template Name" := 'ITEM';
        ItemJournalLine."Journal Batch Name" := 'DEFAULT';
        ItemJournalLine."Line No." := 10000;
        ItemJournalLine.validate("Item No.", '2000');
        ItemJournalLine.validate("Posting Date", 20181001D);
        ItemJournalLine.validate("Entry Type", ItemJournalLine."Entry Type"::"Positive Adjmt.");
        ItemJournalLine."Document No." := 'UP2000';
        ItemJournalLine.Validate("Gen. Bus. Posting Group", 'DANMARK');
        ItemJournalLine.Validate("Gen. Prod. Posting Group", 'DETAIL');
        ItemJournalLine.validate("Inventory Posting Group", 'HANDEL');
        ItemJournalLine.validate(Quantity, 100000);
        ItemJournalLine.Insert;
        ItemJnlPostBatch.Run(ItemJournalLine);

        clear(ItemJournalLine);
        ItemJournalLine."Journal Template Name" := 'ITEM';
        ItemJournalLine."Journal Batch Name" := 'DEFAULT';
        ItemJournalLine."Line No." := 20000;
        ItemJournalLine.validate("Item No.", '2001');
        ItemJournalLine.validate("Posting Date", 20181001D);
        ItemJournalLine.validate("Entry Type", ItemJournalLine."Entry Type"::"Positive Adjmt.");
        ItemJournalLine."Document No." := 'UP2001';
        ItemJournalLine.Validate("Gen. Bus. Posting Group", 'DANMARK');
        ItemJournalLine.Validate("Gen. Prod. Posting Group", 'DETAIL');
        ItemJournalLine.validate("Inventory Posting Group", 'HANDEL');
        ItemJournalLine.validate(Quantity, 100000);
        ItemJournalLine.Insert;
        ItemJnlPostBatch.Run(ItemJournalLine);

        CreateProdOrder();
        Message('Done');
    end;

    procedure CreateProdOrder()
    var
        ProdOrd: Record "Production Order";
        ProdLine: Record "Prod. Order Line";
        i: Integer;

    begin
        i := 0;
        repeat
        begin
            clear(ProdOrd);
            ProdOrd."No." := '';
            ProdOrd.Insert(true);
            ProdOrd.Status := ProdOrd.Status::Released;
            ProdOrd.validate("Source Type", ProdOrd."Source Type"::Item);
            ProdOrd.Validate("Source No.", '2002');
            ProdOrd.Validate(Quantity, 12);
            ProdOrd.Insert(true);
            Clear(ProdLine);
            ProdLine.Status := ProdLine.Status::Released;
            ProdLine."Prod. Order No." := ProdOrd."No.";
            ProdLine."Line No." := 10000;
            ProdLine.Validate("Item No.", '2002');
            ProdLine.insert(true);
            i := i + 1;
        end
        until i = 100;

        Message('Done');

    End;
}